"use strict"

/**
 * Runs the setup function to the DOMContentLoaded event.
 */

document.addEventListener ("DOMContentLoaded", setup);

function setup () {

    /**
     * The object container that stores cache data
     * @type {EpicApplication}
     */
    let EpicApplication = {
        types : ["natural", "enhanced", "aerosol", "cloud"],
        images: [],
        imagesCache: {},
        datesCache: {},
    };

    EpicApplication.types.forEach (type => {
        EpicApplication.datesCache[type] = null;
        EpicApplication.imagesCache[type] = new Map ();
    });

    const selectElement = document.querySelector("#type");
    let dateInput = document.querySelector('#date');
    restrictDates('natural', dateInput);
    generateOptions(EpicApplication.types, selectElement);

    /**
     * Event listener that tracks if user clicked on the image list
     */
    let type = document.querySelector('#type').value;
    const ulist = document.querySelector ('#image-menu') ;
    ulist.addEventListener('click', (event) => {
        let target = event.target.parentNode;
        let clickedObj = EpicApplication.images[target.getAttribute('data-image-list-index')];
        let imageURL = urlForImage(clickedObj, type);

        let dateDiv = document.querySelector('#earth-image-date');
        let captionDiv = document.querySelector('#earth-image-title');
        setHeaders(clickedObj.date, clickedObj.caption, dateDiv, captionDiv, );

        let image = document.querySelector('#earth-image');
        setImage(imageURL, image);
    });


    /**
     *  Event Listener that tracks if user changes image type
     */
    const select = document.querySelector('#type');
    select.addEventListener('change', (e) => {
        let imageType = document.querySelector('#type').value;
        restrictDates(imageType, dateInput);
    });

    /**
     *  Enevt listener that tracks if user clicked submit button
     */
    const form = document.querySelector('#request_form');
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        EpicApplication.images = [];
        type = document.querySelector('#type').value;
        const template = document.querySelector('#image-menu-item');
        let date = document.querySelector('#date').value;

        if (EpicApplication.imagesCache[type].has(date)) {
            EpicApplication.images = EpicApplication.imagesCache[type].get(date);
            addImageToList(EpicApplication.images, ulist, template);
        }
        else {
            fetch (`https://epic.gsfc.nasa.gov/api/${type}/date/${date}`)
        .then(response =>  {
            if (response.ok) {
                console.log ("Response : SUCCESS", response);
            }
            else {
                console.log ("Response : FAILLURE", {cause : response});
                addNoImageMessage(ulist, template);
            }
            return response.json();
        })
        .then(data => {
            if (data.length === 0 || data.length === null){
                addNoImageMessage(ulist, template);
            }
            else{
                data.forEach(element => {
                    EpicApplication.images.push(element);
                });
                addImageToList (EpicApplication.images, ulist, template);
                EpicApplication.imagesCache[type].set(date, EpicApplication.images);
            }
        })
        .catch(error => console.log('Error!', error));
        }
    });

    /**
     * Generates options for a <select> element based on the given types.
     * @param {Array} array - The array containing types for the options.
     * @param {HTMLSelectElement} selectElement - The <select> element to which options will be added.
     */

    function generateOptions (array, selectElement) {
        array.forEach (element => {
            let optionElement  = document.createElement('option');
            optionElement.setAttribute('value', element);
            optionElement.textContent = element;
            selectElement.appendChild(optionElement);
        });
    }
    
    /**
     * Fetches the available dates for the given image type and updates the date input field accordingly.
     * @param {string} type 
     */
    
    function restrictDates (type, date) {
        if (EpicApplication.datesCache[type] !== null) {
            date.max = EpicApplication.datesCache[type];
        }
        else {
            fetch (`https://epic.gsfc.nasa.gov/api/${type}/all`)
        .then(response =>  {
            if (response.ok) {
                console.log ("Response : SUCCESS", response);
            }
            else {
                console.log ("Response : FAILLURE", {cause : response});
            }
            return response.json();
        })
        .then(dates => {
            let listOfDates = [];
            dates.forEach(obj => {
                listOfDates.push(obj.date.toString());
            });
            let sortedDates = listOfDates.sort((a, b) => b - a);
            let latestDate = sortedDates[0];
            dateInput.max = latestDate;
            EpicApplication.datesCache[type] = latestDate;
        })
        .catch(error => console.log('Error!', error))
        }
    }

            /**
     * Adds a message to the list indicating that no images were found. 
     * @param {HTMLUListElement} ulist 
     * @param {HTMLTemplateElement} template 
     */
    
            function addNoImageMessage (ulist, template) {
                ulist.textContent = undefined;
                const content = template.content;
                let clone = template.content.cloneNode(true);
                let list = clone.querySelector('li');
                const span = list.querySelector('.image-list-title');
                list.removeChild(span);
                list.textContent = "No images found!";
                ulist.appendChild(clone);
            }
            
            /**
             * Add headers of images to the list.
             * @param {Array} array 
             * @param {HTMLUListElement} ulist 
             * @param {HTMLTemplateElement} template 
             */
            
            function addImageToList (array, ulist, template) {
                ulist.textContent = undefined;
                for (let i = 0; i < array.length; i++){
                    let clone = template.content.cloneNode(true);
                    let list = clone.querySelector('li');
                    list.setAttribute('data-image-list-index', i);
                    let span = clone.querySelector('.image-list-title');
                    span.textContent = array[i].date;
                    ulist.appendChild(clone);
                }
            }
            
            /**
             * Constructs the URL for the given image object and type.
             * @param {Object} obj 
             * @param {string} type 
             * @returns {string} 
             */
            
            function urlForImage (obj, type) {
                let imgDate = obj.date.split(' ');
                imgDate = imgDate[0];
                let formatedDate = imgDate.replace(/-/g, '/');
                let url = `https://epic.gsfc.nasa.gov/archive/${type}/${formatedDate}/jpg/${obj.image}.jpg`;
                return url;
            }
            
            /**
             * Sets the date and caption headers for the displayed image.
             * @param {string} date  
             * @param {string} caption 
             * @param {HTMLElement} dateDiv 
             * @param {HTMLElement} captionDiv 
             */
            
            function setHeaders (date, caption, dateDiv, captionDiv) {
                dateDiv.textContent = "";
                let h2 = document.createElement('h2');
                h2.textContent = date;
                dateDiv.appendChild(h2);
            
                captionDiv.textContent = "";
                let paragraph = document.createElement('p');
                paragraph.textContent = caption;
                captionDiv.appendChild(paragraph);
            }
            
            function setImage (url, image) {
                image.src = url;
            }
    
    
    
}


